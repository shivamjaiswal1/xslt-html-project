<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="xlink mml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xlink="http://www.w3.org/1999/xlink">
    <xsl:output method="html" />
    
        <xsl:template match="/article">
        <html>
            <!-- article head -->
            <head>
                <style type="text/css">
                    .ProgressBar_progressBarBase__1njSY{height:.5rem;border-radius:10rem}.ProgressBar_progressBarWrap__1dSL2{position:relative;width:100%;background:hsla(0,0%,98%,.5)}.ProgressBar_progressBar__14M3v{background:#fff}
                </style>
                <style type="text/css">
                    .styles_content__nx_52,.styles_wrap__6_qdv{display:flex;flex-direction:column}.styles_wrap__6_qdv{height:360px;border-radius:6px;background-color:#fff;cursor:pointer;box-shadow:0 2px 25px 0 rgba(0,0,0,.1);transition:box-shadow .3s,transform .3s;position:relative;width:100%}.styles_wrap__6_qdv:hover{box-shadow:0 .2rem 4rem 0 rgba(0,0,0,.15);transform:translate3d(0,-1rem,0)}.styles_image__32Tdo{min-height:14.8rem;width:100%;border-top-left-radius:6px;border-top-right-radius:6px;background-size:cover;background-position:50% 50%;background-repeat:no-repeat;background-color:var(--color-accent)}.styles_content__nx_52{box-sizing:border-box;height:100%;width:100%;padding:1.5rem 3.5rem 2rem 2.5rem;position:relative}.styles_title__NDffz{max-height:9.45rem;height:9.45rem;font-size:1.8rem;line-height:1.33;font-weight:700;color:#313537;overflow-wrap:break-word;margin-top:.5rem;overflow:hidden}.styles_titleClamp__1-eSv>div{display:none!important}.styles_score__2FWHt{font-weight:700;color:#313537}.styles_scoreWrap__3GrXD{margin-top:.5rem;color:#666;font-size:1.2rem}.styles_lessonsInfo__SbVMH{font-size:1.2rem;letter-spacing:.05rem;position:absolute;bottom:2rem;color:#313537}.styles_pipe__1XBpq{margin:0 .25rem}.styles_lpIcon__3XeRQ{display:inline-flex;margin-right:.4rem;color:#adaeaf}.styles_imageOverlay__RSsiC{position:absolute;width:100%;min-height:14.8rem}.styles_imageOverlayStarted__17CIL{min-height:14.8rem;width:100%;opacity:.15;background-image:linear-gradient(180deg,transparent,rgba(0,0,0,.83),#000)}.styles_imageOverlayAssignedToMe__1pYS_{min-height:14.8rem;width:100%;opacity:.5;background-image:linear-gradient(180deg,transparent,#000)}.styles_statusText__1q5C8{display:flex;color:#fff;align-items:center;position:absolute;bottom:2.5rem;left:1rem}.styles_statusTextNoProgress__39xCR{bottom:1.2rem}.styles_status__1VLZS{padding-left:.5rem;text-transform:capitalize}.styles_completedStatus__A0RxX{display:flex;color:#fff;align-items:center;position:absolute;top:1rem;right:1rem}.styles_learningPathBadge__3vO-1{display:flex;color:#313537;border-radius:.6rem;padding:.4rem .6rem;font-size:8px;text-transform:uppercase;letter-spacing:.3px;font-weight:700;background-color:#fff;align-items:center;position:absolute;top:1rem;left:1rem}.styles_progressBarWrap__24ETA{position:absolute;left:0;bottom:1rem;width:calc(100% - 2rem);margin:0 1rem}.styles_heartWrap__217al{position:absolute;top:1rem;right:1rem;width:2rem;height:2rem;display:flex;justify-content:center;align-items:center}.styles_heartWrap__217al:hover{cursor:pointer}.styles_heartWrap__217al:hover .styles_heartFilled__35Shx{color:var(--color-accent-focus)}.styles_heartWrap__217al:hover .styles_heartOutline__2fakg{color:#313537}.styles_heartFilled__35Shx{color:var(--color-accent)}.styles_heartOutline__2fakg{color:#979797}.styles_completedAt__2Iu7h{margin-top:1rem;color:#6a737c;font-size:1.2rem}.styles_due__SPTRi{display:inline-block;font-size:1rem;font-weight:900;letter-spacing:.05rem;text-transform:uppercase;line-height:1.2;color:#313537;border:1px solid #979797;padding:.5rem .7rem;border-radius:.4rem}.styles_dueUrgent__1j39c{color:var(--color-accent);border-color:var(--color-accent)}.styles_downloadCertWrap__1wv7-{position:absolute;bottom:2rem}@media (max-width:580px){.styles_progressBarWrap__24ETA{bottom:1.5rem;margin:0 2rem;width:calc(100% - 4rem)}.styles_statusText__1q5C8{left:2rem}}
                </style>
                <style type="text/css">.IconLearningPath_defaultFill__3ksqg{color:#adaeaf}</style>
                <style type="text/css">.IconError_iconError__1pbVY{color:#d0021b}</style>
                <style type="text/css">.styles_image__32Tdo{display:inline-block}</style>
                <style class="vjs-styles-defaults">
                    .video-js {
                    width: 300px;
                    height: 150px;
                    }

                    .vjs-fluid {
                    padding-top: 56.25%
                    }
                </style>
                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
                <link type="text/css" rel="stylesheet" href="lib/icomoon.css"/>
                <link type="text/css" rel="stylesheet" href="lib/main.bundle.css"/>
                <script type="text/javascript" src="lib/player-0.0.11.min.js"></script>
                <script type="text/javascript" src="lib/lzwcompress.js"></script>
                <script type="text/javascript">
                    window.resizeTo(screen.width, screen.height);
                </script>
                <style type="text/css">/* Chart.js */
                    @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style>
                <style id="focus-ring-style">:focus{outline: none}</style>
                <meta charset="utf-8"/>
                <title>
                    F1000-Article
                </title>
            </head>

            <!-- article body -->
            <body>
                <style>
                    
      .brand--head, .brand--head *   { font-family: 'Lato' !important; }
      .brand--body, .brand--body *   { font-family: 'Merriweather' !important; }
      .brand--lhead, .brand--lhead * { font-family: 'Lato' !important; }
      .brand--lbody, .brand--lbody * { font-family: 'Merriweather' !important; }
      .brand--ui, .brand--ui *       { font-family: 'Lato' !important; }

      .brand--beforeHead:before { font-family: 'Lato' !important; }
      .brand--afterHead:after { font-family: 'Lato' !important; }

      .brand--background        { background-color: #FF631E !important; }
      .brand--background-all *  { background-color: #FF631E !important; }
      .brand--border            { border-color: #FF631E !important; }
      .brand--color             { color: #FF631E !important; }
      .brand--color-all *       { color: #FF631E !important; }
      .brand--shadow            { box-shadow: 0 0 0 0.2rem #FF631E !important; }
      .brand--shadow-all *      { box-shadow: 0 0 0 0.2rem #FF631E !important; }
      .brand--fill              { fill: #FF631E !important; }
      .brand--stroke            { stroke: #FF631E !important; }

      .brand--beforeBackground:before { background-color: #FF631E !important; }
      .brand--beforeBorder:before     { border-color: #FF631E !important; }
      .brand--beforeColor:before      { color: #FF631E !important; }

      .brand--afterBackground:after { background-color: #FF631E !important; }
      .brand--afterBorder:after     { border-color: #FF631E !important; }
      .brand--afterColor:after      { color: #FF631E !important; }

      /* should be applied to containers where links
         need to appear in brand color */
      .brand--linkColor a { color: #FF631E !important; }

      /* should be applied to containers where links
         need to change to brand color on hover */
      .brand--linkColorHover a:hover { color: #FF631E !important; }

      /* should be applied to any single element that
         needs to change color on hover */
      .brand--hoverColor:hover { color: #FF631E !important; }

      /* TODO: probably need a way to create a 5-10% darker/lighter color */
      .brand--linkColor a:hover,
      .button.brand--background:hover { opacity: .65 !important; }

      @media (min-width: 47.9375em) { /* 768px */
        .brand--min768--background { background-color: #FF631E !important; }
        .brand--min768--border     { border-color: #FF631E !important; }
        .brand--min768--color      { color: #FF631E !important; }
      }

      @media (max-width: 47.9375em) { /* 768px */
        .brand--max768--background { background-color: #FF631E !important; }
        .brand--max768--border     { border-color: #FF631E !important; }
        .brand--max768--color      { color: #FF631E !important; }
      }
    
                </style>
                <div id="app">
                    <div id="innerApp">
                        <div>
                            <style>
  .brand--head, .brand--head *   { font-family: Lato !important; }
  .brand--body, .brand--body *   { font-family: Merriweather !important; }
  .brand--lhead, .brand--lhead * { font-family: Lato !important; }
  .brand--lbody, .brand--lbody * { font-family: Merriweather !important; }
  .brand--ui, .brand--ui *       { font-family: Lato !important; }

  .brand--beforeHead:before { font-family: Lato !important; }
  .brand--afterHead:after   { font-family: Lato !important; }

  .brand--background             { background-color: #FF631E !important; }
  .brand--background-transparent { background-color: #FF631E1A !important; }
  .brand--background-all *       { background-color: #FF631E !important; }
  .brand--border                 { border-color: #FF631E !important; }
  .brand--color                  { color: #FF631E !important; }
  .brand--color-all *            { color: #FF631E !important; }
  .brand--shadow                 { box-shadow: 0 0 0 0.2rem #FF631E !important; }
  .brand--shadow-all *           { box-shadow: 0 0 0 0.2rem #FF631E !important; }
  .brand--fill                   { fill: #FF631E !important; }
  .brand--stroke                 { stroke: #FF631E !important; }


  .brand--beforeBackground:before { background-color: #FF631E !important; }
  .brand--beforeBorder:before     { border-color: #FF631E !important; }
  .brand--beforeColor:before      { color: #FF631E !important; }

  .brand--afterBackground:after { background-color: #FF631E !important; }
  .brand--afterBorder:after     { border-color: #FF631E !important; }
  .brand--afterColor:after      { color: #FF631E !important; }

  /* should be applied to containers where links
     need to appear in brand color */
  .brand--linkColor a {
    color: #FF631E !important;
    fill: #FF631E !important;
  }

  /* should be applied to containers where links
     need to change to brand color on hover */
  .brand--linkColorHover a:hover {
    color: #FF631E !important;
    fill: #FF631E !important;
  }

  /* should be applied to any single element that
     needs to change color on hover */
  .brand--hoverColor:hover { color: #FF631E !important; }

  .brand--linkColor a:hover,
  .button.brand--background:hover { opacity: .65 !important; }

  .brandHover:hover .brandHover__target--fill { fill: #FF631E !important; }

  @media (min-width: 47.9375em) { /* 768px */
    .brand--min768--background { background-color: #FF631E !important; }
    .brand--min768--border     { border-color: #FF631E !important; }
    .brand--min768--color      { color: #FF631E !important; }
  }

  @media (max-width: 47.9375em) { /* 768px */
    .brand--max768--background { background-color: #FF631E !important; }
    .brand--max768--border     { border-color: #FF631E !important; }
    .brand--max768--color      { color: #FF631E !important; }
  }
</style>
                        </div>
                        <div class="transition-group">
                            <div class="transition-group">
                                <div class="lesson" data-lesson="true">
                                    <section aria-hidden="false" aria-label="Course Overview" data-lesson-side="true" class="lesson__sidebar" id="courseOverviewSidebar">
                                        <div id="overview-sidebar-container" class="overview-sidebar" data-overview-sidebar="true"><div class="overview-sidebar__header-wrap brand--head" data-overview-sidebar-header-wrap="true"><section aria-hidden="true" aria-label="Course Information" class="overview-sidebar__header-outer" style="max-height: 169px;"><div></div><div class="overview-sidebar__header brand--background"><div class="overview-sidebar__header--background"></div><div><button aria-label="Show Search Bar" class="overview-sidebar__search-icon overview-sidebar__search-icon-enter-done" data-focus-name="showSearch" tabindex="-1" type="button"><svg viewBox="0 0 15.6 14.7" width="16" height="16" class="i i-search" focusable="false"><title>Search</title><desc>Magnifying glass</desc><path d="M15.2,12.8l-2.9-2.9c0.5-1,0.9-2,0.9-3.2c0-3.6-2.9-6.5-6.5-6.5c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5c1.7,0,3.2-0.6,4.4-1.7l2.8,2.8c0.4,0.4,1,0.4,1.4,0C15.6,13.8,15.6,13.2,15.2,12.8z M6.7,11.2c-2.5,0-4.5-2-4.5-4.5s2-4.5,4.5-4.5s4.5,2,4.5,4.5S9.1,11.2,6.7,11.2z"></path></svg></button></div><div class="overview-sidebar__info" tabindex="-1"><a class="overview-sidebar__title" data-focus-name="title" tabindex="-1" href="#/">F1000 </a><button aria-label="SKIP TO LESSON" class="overview-sidebar__skip-button overview-sidebar__skip-button--offscreen" data-focus-name="skip" tabindex="-1">SKIP TO LESSON</button><div class="overview-sidebar__progress brand--ui"><div class="progress-bar"><div class="progress-bar__percentage-top">100% COMPLETE</div><div class="progress-bar__line"><div class="progress-bar__fill" style="width: 100%;"></div></div><div class="progress-bar__percentage-bottom">100% COMPLETE</div></div></div></div></div></section><section aria-hidden="true" aria-label="Course Information" class="overview-sidebar__header-outer overview-sidebar__header--clone" style="max-height: none; width: 260px;"><div></div><div class="overview-sidebar__header brand--background"><div class="overview-sidebar__header--background"></div><div><button aria-label="Show Search Bar" class="overview-sidebar__search-icon overview-sidebar__search-icon-enter-done" data-focus-name="showSearch" tabindex="-1" type="button"><svg viewBox="0 0 15.6 14.7" width="16" height="16" class="i i-search" focusable="false"><title>Search</title><desc>Magnifying glass</desc><path d="M15.2,12.8l-2.9-2.9c0.5-1,0.9-2,0.9-3.2c0-3.6-2.9-6.5-6.5-6.5c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5c1.7,0,3.2-0.6,4.4-1.7l2.8,2.8c0.4,0.4,1,0.4,1.4,0C15.6,13.8,15.6,13.2,15.2,12.8z M6.7,11.2c-2.5,0-4.5-2-4.5-4.5s2-4.5,4.5-4.5s4.5,2,4.5,4.5S9.1,11.2,6.7,11.2z"></path></svg></button></div><div class="overview-sidebar__info" tabindex="-1"><a class="overview-sidebar__title" data-focus-name="title" tabindex="-1" href="#/">F1000 </a><button aria-label="SKIP TO LESSON" class="overview-sidebar__skip-button overview-sidebar__skip-button--offscreen" data-focus-name="skip" tabindex="-1">SKIP TO LESSON</button><div class="overview-sidebar__progress brand--ui"><div class="progress-bar"><div class="progress-bar__percentage-top">100% COMPLETE</div><div class="progress-bar__line"><div class="progress-bar__fill" style="width: 100%;"></div></div><div class="progress-bar__percentage-bottom">100% COMPLETE</div></div></div></div></div></section></div><div class="overview-sidebar__container"><div class="overview-sidebar__content" data-overview-sidebar-content="true"><div class="progress-bar"><div class="progress-bar__percentage-top">100% COMPLETE</div><div class="progress-bar__line"><div class="progress-bar__fill" style="width: 100%;"></div></div><div class="progress-bar__percentage-bottom">100% COMPLETE</div></div><nav aria-label="Course Outline" class="lesson-lists"><ol class="lesson-lists__list"><li class="lesson-lists__item" style="transition-delay: 0s;"><a aria-current="page" class="lesson-link lesson-link--visited lesson-link--active brand--beforeBackground" tabindex="-1" href="#/lessons/KHtrMu_9gLuwOkb3Ov-vaKAD0_nUe2z3"><div aria-hidden="true" class="lesson-link__icon"><svg viewBox="0 0 16 12" width="16" height="12" class="i" focusable="false"><title>Align left</title><desc>Three vertical lines aligned to the left</desc><path d="M8.4148147,12 L1.05185184,12 C0.471229623,12 0,11.552 0,11 C0,10.448 0.471229623,10 1.05185184,10 L8.4148147,10 C8.99543692,10 9.46666654,10.448 9.46666654,11 C9.46666654,11.552 8.99543692,12 8.4148147,12"></path><path d="M12.2500002,2 L0.816666683,2 C0.365866674,2 0,1.552 0,1 C0,0.448 0.365866674,0 0.816666683,0 L12.2500002,0 C12.7008002,0 13.0666669,0.448 13.0666669,1 C13.0666669,1.552 12.7008002,2 12.2500002,2"></path><path d="M15,7 L1,7 C0.448,7 0,6.552 0,6 C0,5.448 0.448,5 1,5 L15,5 C15.552,5 16,5.448 16,6 C16,6.552 15.552,7 15,7"></path></svg></div><div class="lesson-link__name brand--ui"><span aria-hidden="true">Article</span><span class="visually-hidden">Article 100 Percent Complete</span></div></a><div class="lesson-link__progress"><span aria-hidden="true"><svg xmlns="https://www.w3.org/2000/svg" version="1.1" aria-labelledby="pc-t-9BT7REkhoZzwQVm3" aria-describedby="pc-d-9BT7REkhoZzwQVm3" focusable="false" height="21" role="img" style="display: inline-block; vertical-align: middle;" width="21"><title id="pc-t-9BT7REkhoZzwQVm3">100% complete</title><desc id="pc-d-9BT7REkhoZzwQVm3">A circle with a colored border representing one's progress through a lesson.</desc><circle cx="10.5" cy="10.5" fill="transparent" r="8.5" stroke-width="2" style="stroke: rgba(49, 53, 55, 0.1); transition: opacity 0.3s ease 0.3s; opacity: 0; visibility: hidden;" transform="rotate(-89.9 10.5 10.5)"></circle><circle class="brand--stroke brand--fill-opacity brand--fill" cx="10.5" cy="10.5" fill="transparent" r="8.5" stroke-dasharray="53.407" stroke-dashoffset="0" stroke-width="2" style="stroke: rgb(80, 171, 241); transition: stroke-dashoffset 0.4s ease-out 0s, fill 0.3s ease 0.3s; fill: rgb(80, 171, 241);" transform="rotate(-89.9 10.5 10.5)"></circle><path d="M8.64,0L9.9,1.17L3.51,7.2L0,3.87L1.26,2.7l2.34,2.16L8.64,0z" fill="#fff" style="transition: opacity 0.3s ease 0.3s;" transform="translate(5.5 7)"></path></svg></span></div></li></ol></nav></div></div></div>
                                    
                                    </section>
                                    <div class="lesson__wrap"></div>
                                    <div class="lesson__content">
                        <div class="page-view page-view--visible">
                            <div class="page-wrap" id="page-wrap">
                                <div>
                                    <main aria-label="Lesson Content">
                                        <div class="page" data-type="blocks" data-page="true">
                                            <div class="previous-lesson brand--linkColorHover" data-previous-lesson="true" style="display: flex;"><a class="previous-lesson__link" href="#/"><i class="icon icon-chevron-up"></i><div class="previous-lesson__title brand--ui">Home</div></a></div>
                                        </div>
                                        <div class="page__wrapper page__wrapper--white">
                                            <div class="page__menu">
                                                <button aria-expanded="false" aria-label="Toggle Course Overview" aria-controls="courseOverviewSidebar" class="button button--default button--menu" tabindex="0"><i class="button__icon icon-menu"></i>
                                                </button><div id="no-menu" tabindex="-1">
                                                </div>
                                            </div>
                                            <section class="page__header" aria-label="Lesson Header">
                                                <div class="page__header-limit">
                                                    <div class="page__header-container">
                                                        <div class="lesson-header__row">
                                                            <div class="lesson-header__main">
                                                                <div class="lesson-header">
                                                                    <div class="lesson-header__top-wrap brand--afterBackground">
                                                                        <div aria-hidden="false" class="lesson-header__title brand--lhead brand--linkColor">
                                                                            <h1 class="fr-view">Article</h1>
                                                                        </div>
                                                                        <div class="lesson-header__counter brand--body brand--linkColor">
                                                                            <div class="lesson-header__count">Lesson 1 of 1</div>
                                                                        </div>
                                                                    </div>
                                                                    <div aria-hidden="false" class="lesson-header__description brand--lbody brand--linkColor brand--linkColor">
                                                                        <div class="fr-view">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="progress-wrap">
                                                            <div class="progress">
                                                                <div class="progress__container">
                                                                    <div class="progress__indicator brand--background" style="transform: translate3d(13%, 0px, 0px);">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <div>
                                                <section aria-label="Lesson Content" class="blocks-lesson">
                                                    <xsl:apply-templates select="front/article-meta"/>
                                                    <xsl:apply-templates select="body"/>
                                                    <div class="noOutline" tabindex="-1"></div>
                                                </section>
                                            </div>
                                            
                                        </div>
                                    </main>
                                </div>
                            </div>
                        </div>
                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    
  (function(root) {
    window.labelSet = {"id":"8t7Tg668sZpHJE-6AJt5OI5G","author":"aid|cb43f53d-b9b6-428d-92c8-26f8e3cbe5f1","name":"English","defaultId":1,"defaultSet":true,"labels":{"result":"result","search":"search","results":"results","quizNext":"NEXT","tabGroup":"Tab","codeGroup":"Code","noResults":"No results for","noteGroup":"Note","quizScore":"Your score","quizStart":"START QUIZ","courseExit":"EXIT COURSE","courseHome":"Home","lessonName":"Lesson","quizSubmit":"SUBMIT","quoteGroup":"Quote","salutation":"👋 Bye!","buttonGroup":"Button","courseStart":"START COURSE","embedViewOn":"VIEW ON","exitMessage":"You may now leave this page.","quizCorrect":"Correct","quizPassing":"PASSING","quizResults":"Quiz Results","courseResume":"RESUME COURSE","processStart":"START","processSwipe":"Swipe to continue","quizContinue":"Continue","quizLandmark":"Quiz","quizQuestion":"Question","courseDetails":"DETAILS","embedReadMore":"Read more","feedbackGroup":"Feedback","quizIncorrect":"Incorrect","quizTakeAgain":"TAKE AGAIN","sortingReplay":"REPLAY","accordionGroup":"Accordion","embedLinkGroup":"Embedded web content","lessonComplete":"COMPLETE","statementGroup":"Statement","storylineGroup":"Storyline","attachmentGroup":"File Attachment","embedPhotoGroup":"Embedded photo","embedVideoGroup":"Embedded video","playButtonLabel":"Play","processComplete":"Complete","processLandmark":"Process","processNextStep":"NEXT STEP","processStepName":"Step","seekSliderLabel":"Seek","sortingLandmark":"Sorting Activity","audioPlayerGroup":"Audio player. You can use the space bar to toggle playback and arrow keys to scrub.","buttonStackGroup":"Button stack","embedPlayerGroup":"Embedded media player","lessonRestricted":"Lessons must be completed in order","pauseButtonLabel":"Pause","scenarioComplete":"Scenario Complete!","scenarioContinue":"CONTINUE","scenarioTryAgain":"TRY AGAIN","textOnImageGroup":"Text on image","timelineLandmark":"Timeline","urlEmbedLandmark":"URL/Embed","videoPlayerGroup":"Video player. You can use the space bar to toggle playback and arrow keys to scrub.","blocksClickToFlip":"Click to flip","blocksPreposition":"of","bulletedListGroup":"Bulleted list","checkboxListGroup":"Checkbox list","imageAndTextGroup":"Image and text","imageGalleryGroup":"Image gallery","lessonPreposition":"of","numberedListGroup":"Numbered list","processLessonName":"Lesson","processStartAgain":"START AGAIN","scenarioStartOver":"START OVER","courseSkipToLesson":"SKIP TO LESSON","flashcardBackLabel":"Flashcard back","flashcardGridGroup":"Flashcard grid","nextFlashCardLabel":"Next flashcard","flashcardFrontLabel":"Flashcard front","flashcardStackGroup":"Flashcard stack","knowledgeCheckGroup":"Knowledge check","sortingCardsCorrect":"Cards Correct","hamburgerButtonLabel":"Course Overview Sidebar","lessonHeaderLandmark":"Lesson Header","numberedDividerGroup":"Numbered divider","lessonContentLandmark":"Lesson Content","lessonSidebarLandmark":"Lesson Sidebar","quizAnswerPlaceholder":"Type your answer here","labeledGraphicLandmark":"Labeled Graphic","previousFlashCardLabel":"Previous flashcard","processStepPreposition":"of","overviewPageTitleSuffix":"Overview","quizAcceptableResponses":"Acceptable responses","quizRequirePassingScore":"Must pass quiz before continuing","timelineCardGroupPrefix":"Timeline Card","labeledGraphicBubbleLabel":"Labeled graphic bubble","labeledGraphicMarkerLabel":"Labeled graphic marker","labeledGraphicNextMarkerLabel":"Next marker","labeledGraphicPreviousMarkerLabel":"Previous marker"},"deleted":false,"createdAt":"2020-10-16T10:43:13.556Z","updatedAt":"2020-10-16T10:43:13.556Z","iso639Code":"en","transferredAt":null};
    window.courseData = "eyJjb3Vyc2UiOnsiaWQiOiJYcHl5X1JhQTdtWGIyYk4zNk1tZjBvNWp2dHU2Z3ExYSIsIm9yaWdpbmFsSWQiOm51bGwsImF1dGhvciI6ImFpZHxjYjQzZjUzZC1iOWI2LTQyOGQtOTJjOC0yNmY4ZTNjYmU1ZjEiLCJzZWxlY3RlZEF1dGhvcklkIjoiIiwiY29sb3IiOiIjRkY2MzFFIiwibmF2aWdhdGlvbk1vZGUiOiJmcmVlIiwidGl0bGUiOiJGMTAwMCAiLCJzaGFyZVBhc3N3b3JkIjoiIiwiZGVzY3JpcHRpb24iOiIiLCJzaGFyZUlkIjoiQ3B1eVhxT2xaV1lNR2h2LVItVXRJa1FCVnRKblRNcUgiLCJjb3B5T2YiOiIiLCJvcmRlciI6IjE2MDMxMDYzMDU2MzgiLCJmb250cyI6bnVsbCwibWVkaWEiOnt9LCJjb3ZlckltYWdlIjp7fSwibGVzc29ucyI6W3siaWQiOiJLSHRyTXVfOWdMdXdPa2IzT3YtdmFLQUQwX25VZTJ6MyIsIm9yaWdpbmFsSWQiOm51bGwsImF1dGhvciI6ImFpZHxjYjQzZjUzZC1iOWI2LTQyOGQtOTJjOC0yNmY4ZTNjYmU1ZjEiLCJzZWxlY3RlZEF1dGhvcklkIjoibm9uZSIsImNvdXJzZUlkIjoiWHB5eV9SYUE3bVhiMmJOMzZNbWYwbzVqdnR1NmdxMWEiLCJ0aXRsZSI6IkFydGljbGUiLCJkZXNjcmlwdGlvbiI6IiIsInR5cGUiOiJibG9ja3MiLCJpY29uIjoiQXJ0aWNsZSIsIml0ZW1zIjpbeyJpZCI6ImNrZ2dnNG00bzAwMnozYTYxemt3bHhpcnIiLCJ0eXBlIjoidGV4dCIsIml0ZW1zIjpbeyJpZCI6ImNrZ2dnMm1xZDAwMjQzYTYxbjV0cjlsdHgiLCJoZWFkaW5nIjoiPHA+PHN0cm9uZz5DT1ZJRC0xOSB0ZXN0aW5nIGNhcGFiaWxpdGllcyBhdCB1cmdlbnQgY2FyZSBjZW50ZXJzIGluIHN0YXRlcyB3aXRoIHRoZSBncmVhdGVzdCBkaXNlYXNlIGJ1cmRlbiZuYnNwOzwvc3Ryb25nPjwvcD4iLCJwYXJhZ3JhcGgiOiI8cD5bdmVyc2lvbiAxOyBwZWVyIHJldmlldzogMSBhcHByb3ZlZCwgMSBhcHByb3ZlZCB3aXRoIHJlc2VydmF0aW9uc10mbmJzcDs8L3A+PHA+PGJyPjwvcD48cD48YnI+PC9wPiJ9XSwiZmFtaWx5IjoidGV4dCIsInZhcmlhbnQiOiJoZWFkaW5nIHBhcmFncmFwaCIsInNldHRpbmdzIjp7InBhZGRpbmdUb3AiOjMsInBhZGRpbmdCb3R0b20iOjMsImJhY2tncm91bmRDb2xvciI6IiIsImVudHJhbmNlQW5pbWF0aW9uIjp0cnVlfX0seyJpZCI6ImNrZ2dnaDQ5NzAwMzMzYTYxa3RtM3k0NzciLCJ0eXBlIjoidGV4dCIsIml0ZW1zIjpbeyJpZCI6ImNrZ2dnMm1xZDAwMjQzYTYxbjV0cjlsdHgiLCJoZWFkaW5nIjoiPHA+QWJzdHJhY3Q8L3A+IiwicGFyYWdyYXBoIjoiPHA+V2hpbGUgcmFwaWQgYW5kIGFjY2Vzc2libGUgZGlhZ25vc2lzIGlzIHBhcmFtb3VudCB0byBtb25pdG9yaW5nIGFuZCByZWR1Y2luZyB0aGUgc3ByZWFkIG9mIGRpc2Vhc2UsIENPVklELTE5IHRlc3RpbmcgY2FwYWJpbGl0aWVzIGFjcm9zcyB0aGUgVS5TLiByZW1haW4gY29uc3RyYWluZWQuIEZvciBtYW55IGluZGl2aWR1YWxzLCB1cmdlbnQgY2FyZSBjZW50ZXJzIChVQ0NzKSBtYXkgb2ZmZXIgdGhlIG1vc3QgYWNjZXNzaWJsZSBhdmVudWUgdG8gYmUgdGVzdGVkLiBUaHJvdWdoIGEgcGhvbmUgc3VydmV5LCB3ZSBkZXNjcmliZSB0aGUgQ09WSUQtMTkgdGVzdGluZyBjYXBhYmlsaXRpZXMgYXQgVUNDcyBhbmQgcHJvdmlkZSBhIHNuYXBzaG90IGhpZ2hsaWdodGluZyB0aGUgbGltaXRlZCBDT1ZJRC0xOSB0ZXN0aW5nIGNhcGFiaWxpdGllcyBhdCBVQ0NzIGluIHN0YXRlcyB3aXRoIHRoZSBncmVhdGVzdCBkaXNlYXNlIGJ1cmRlbiZuYnNwOzwvcD4ifV0sImZhbWlseSI6InRleHQiLCJ2YXJpYW50IjoiaGVhZGluZyBwYXJhZ3JhcGgiLCJzZXR0aW5ncyI6eyJwYWRkaW5nVG9wIjozLCJwYWRkaW5nQm90dG9tIjozLCJiYWNrZ3JvdW5kQ29sb3IiOiIiLCJlbnRyYW5jZUFuaW1hdGlvbiI6dHJ1ZX19LHsiaWQiOiJja2dnZ2lpejcwMDM0M2E2MXJobjNtMjQ0IiwidHlwZSI6InRleHQiLCJpdGVtcyI6W3siaWQiOiJja2dnZzJtcWQwMDI0M2E2MW41dHI5bHR4IiwiaGVhZGluZyI6IjxwPktleXdvcmRzPC9wPiIsInBhcmFncmFwaCI6IjxwPkNPVklELTE5LCB1cmdlbnQgY2FyZSBjZW50ZXIsIHRlc3RpbmcsIGhlYWx0aCBzZXJ2aWNlcyZuYnNwOzwvcD4ifV0sImZhbWlseSI6InRleHQiLCJ2YXJpYW50IjoiaGVhZGluZyBwYXJhZ3JhcGgiLCJzZXR0aW5ncyI6eyJwYWRkaW5nVG9wIjozLCJwYWRkaW5nQm90dG9tIjozLCJiYWNrZ3JvdW5kQ29sb3IiOiIiLCJlbnRyYW5jZUFuaW1hdGlvbiI6dHJ1ZX19LHsiaWQiOiJja2dnZ2oxdWowMDM1M2E2MWJ0bWs1ZjhsIiwidHlwZSI6InRleHQiLCJpdGVtcyI6W3siaWQiOiJja2dnZzJtcWQwMDI0M2E2MW41dHI5bHR4IiwiaGVhZGluZyI6IjxwPkludHJvZHVjdGlvbjwvcD4iLCJwYXJhZ3JhcGgiOiI8cD5XaGlsZSByYXBpZCBhbmQgYWNjZXNzaWJsZSBDT1ZJRC0xOSBkaWFnbm9zaXMgaXMgcGFyYW1vdW50IHRvIG1vbml0b3JpbmcgYW5kIHJlZHVjaW5nIHRoZSBzcHJlYWQgb2YgZGlzZWFzZSwgQ09WSUQtMTkgdGVzdGluZyBjYXBhYmlsaXRpZXMgYWNyb3NzIHRoZSBVLlMuIHJlbWFpbiBjb25zdHJhaW5lZC4gRm9yIG1hbnkgaW5kaXZpZHVhbHMsIHVyZ2VudCBjYXJlIGNlbnRlcnMgKFVDQ3MpIG1heSBvZmZlciB0aGUgbW9zdCBhY2Nlc3NpYmxlIGF2ZW51ZSB0byBiZSB0ZXN0ZWQuIFVzaW5nIGEgcGhvbmUgc3VydmV5LCB3ZSBkZXNjcmliZSB0aGUgQ09WSUQtMTkgdGVzdGluZyBjYXBhYmlsaXRpZXMgb2YgVUNDcyBpbiBzdGF0ZXMgd2l0aCB0aGUgZ3JlYXRlc3QgZGlzZWFzZSBidXJkZW4uJm5ic3A7PC9wPiJ9XSwiZmFtaWx5IjoidGV4dCIsInZhcmlhbnQiOiJoZWFkaW5nIHBhcmFncmFwaCIsInNldHRpbmdzIjp7InBhZGRpbmdUb3AiOjMsInBhZGRpbmdCb3R0b20iOjMsImJhY2tncm91bmRDb2xvciI6IiIsImVudHJhbmNlQW5pbWF0aW9uIjp0cnVlfX0seyJpZCI6ImNrZ2dnbWdmaTAwMzYzYTYxMzQ0N3VkNmkiLCJ0eXBlIjoidGV4dCIsIml0ZW1zIjpbeyJpZCI6ImNrZ2dnMm1xZDAwMjQzYTYxbjV0cjlsdHgiLCJoZWFkaW5nIjoiPHA+TWV0aG9kczwvcD4iLCJwYXJhZ3JhcGgiOiI8cD5PdXIgc3R1ZHkgcmVjZWl2ZWQgbm9uLWh1bWFuIHJlc2VhcmNoIElSQiBleGVtcHRpb24gZnJvbSB0aGUgWWFsZSBTY2hvb2wgb2YgTWVkaWNpbmUgYW5kIHBhcnRpY2lwYW50IGNvbnNlbnQgd2FzIG5vdCByZXF1aXJlZC4gV2UgaWRlbnRpZmllZCB0ZW4gc3RhdGVzIHdpdGggdGhlIGhpZ2hlc3QgQ09WSUQtMTkgY2FzZWxvYWQgYXMgb2YgTWFyY2ggMTksIDIwMjAgYWNjb3JkaW5nIHRvIHRoZSBDZW50ZXJzIGZvciBEaXNlYXNlIENvbnRyb2wgKENEQykxLiBVc2luZyB0aGUgVXJnZW50IENhcmUgQXNzb2NpYXRpb24gJmxkcXVvO0ZpbmQgYW4gVXJnZW50IENhcmUmcmRxdW87IGRpcmVjdG9yeSwgd2UgaWRlbnRpZmllZCBhbGwgVUNDcyB3aXRoaW4gdGhlIHN0YXRlIG9mIGludGVyZXN0IGFuZCBhc3NpZ25lZCBlYWNoIFVDQyBhIG51bWVyaWMgaWRlbnRpZmllci4gQSByYW5kb20gbnVtYmVyIGdlbmVyYXRvciB3YXMgdXNlZCB0byBzZWxlY3QgZm9yIGEgY29udmVuaWVuY2Ugc2FtcGxlIG9mIDI1IFVDQ3MgcGVyIHN0YXRlLiBJZiB0aGUgVUNDIHdhcyBub3QgYWJsZSB0byBiZSBjb250YWN0ZWQsIGEgbmV3IFVDQyB3YXMgcmFuZG9tbHkgc2VsZWN0ZWQgYW5kIGNhbGxlZC4gVUNDcyB3ZXJlIGNsYXNzaWZpZWQgaW50byBpbmRlcGVuZGVudCwgaG9zcGl0YWwvaGVhbHRoIG5ldHdvcmssIGFuZCBhY2FkZW1pYyBjYXRlZ29yaWVzLjwvcD48cD5Vc2luZyBhIHN0YW5kYXJkaXplZCBzdXJ2ZXkgc2NyaXB0IChGaWd1cmUgMSksIHRyYWluZWQgaW52ZXN0aWdhdG9ycyBhc2tlZCBVQ0MgcmVjZXB0aW9uaXN0cyBhYm91dCBDT1ZJRC0xOSB0ZXN0aW5nIGFiaWxpdHksIHRlc3RpbmcgY3JpdGVyaWEsIHRpbWUgdG8gdGVzdCByZXN1bHRzLCBjb3N0cyBvZiB0ZXN0cyBhbmQgdmlzaXRzIGZvciBpbnN1cmVkL3VuaW5zdXJlZCBwYXRpZW50cywgYW5kIHRlc3QgcmVmZXJyYWxzLiBBbGwgMjUwIGNhbGxzIHdlcmUgbWFkZSBvbiBNYXJjaCAyMCwgMjAyMCBhbmQgd2VyZSBsaW1pdGVkIHRvIDEgbWludXRlIHRvIG1pbmltaXplIG9jY3VweWluZyBjbGluaWMgcmVzb3VyY2VzLjwvcD4ifV0sImZhbWlseSI6InRleHQiLCJ2YXJpYW50IjoiaGVhZGluZyBwYXJhZ3JhcGgiLCJzZXR0aW5ncyI6eyJwYWRkaW5nVG9wIjozLCJwYWRkaW5nQm90dG9tIjozLCJiYWNrZ3JvdW5kQ29sb3IiOiIiLCJlbnRyYW5jZUFuaW1hdGlvbiI6dHJ1ZX19LHsiaWQiOiJja2dnZ3o4dnYwMDNlM2E2MTM0MjEwcGFlIiwidHlwZSI6ImltYWdlIiwiaXRlbXMiOlt7ImlkIjoiY2tnZ2cybXFkMDAyZzNhNjF4bms3ZmU3MCIsIm1lZGlhIjp7ImltYWdlIjp7ImtleSI6InJpc2UvY291cnNlcy9YcHl5X1JhQTdtWGIyYk4zNk1tZjBvNWp2dHU2Z3ExYS9pdHZYanc0RDNPTVFoZUF5LWJiYzIwMDNmLTEyOWItNDA4OS1hZWRhLTdlNzFlOWZhZjdhMl9maWd1cmUxLmdpZiIsInR5cGUiOiJpbWFnZSIsImNydXNoZWRLZXkiOiJrMl9nRkN0MVotc1I0MHY2X3N0ajBlMDdINkxiU3ExNVYuZ2lmIiwib3JpZ2luYWxVcmwiOiJiYmMyMDAzZi0xMjliLTQwODktYWVkYS03ZTcxZTlmYWY3YTJfZmlndXJlMS5naWYiLCJ1c2VDcnVzaGVkS2V5Ijp0cnVlfX0sImNhcHRpb24iOiI8cD5GaWd1cmUgMS48L3A+IiwicGFyYWdyYXBoIjoiIn1dLCJmYW1pbHkiOiJpbWFnZSIsInZhcmlhbnQiOiJoZXJvIiwic2V0dGluZ3MiOnsib3BhY2l0eSI6MC41LCJwYWRkaW5nVG9wIjozLCJ6b29tT25DbGljayI6dHJ1ZSwib3BhY2l0eUNvbG9yIjoiIzAwMDAwMCIsInBhZGRpbmdCb3R0b20iOjMsImJhY2tncm91bmRDb2xvciI6IiIsImVudHJhbmNlQW5pbWF0aW9uIjp0cnVlfX0seyJpZCI6ImNrZ2dnbmQ3cDAwMzczYTYxZzF1ZDAzM3YiLCJ0eXBlIjoidGV4dCIsIml0ZW1zIjpbeyJpZCI6ImNrZ2dnMm1xZDAwMjQzYTYxbjV0cjlsdHgiLCJoZWFkaW5nIjoiPHA+UmVzdWx0czwvcD4iLCJwYXJhZ3JhcGgiOiI8cD5PZiAyNTAgVUNDcyBjb250YWN0ZWQsIDU3ICgyMi44JSkgb2ZmZXJlZCBDT1ZJRC0xOSB0ZXN0aW5nLiBIb3NwaXRhbC9oZWFsdGggbmV0d29yay1hZmZpbGlhdGVkIFVDQ3Mgd2VyZSBtb3JlIGxpa2VseSB0byBvZmZlciBDT1ZJRC0xOSB0ZXN0cyBjb21wYXJlZCB0byBpbmRlcGVuZGVudCBVQ0NzIChvZGRzIHJhdGlvIDMuNjksIDk1JSBjb25maWRlbmNlIGludGVydmFsIDEuOTQmbmRhc2g7Ny4wMSwgcCZsdDswLjAwMDEpLjwvcD48cD5PZiBVQ0NzIHRoYXQgb2ZmZXJlZCB0byB0ZXN0LCA1NiAoOTguMiUpIHJlcXVpcmVkIHRoZSBwYXRpZW50IHRvIGJlIHN5bXB0b21hdGljICh0eXBpY2FsbHkgZmV2ZXIgYW5kIHJlc3BpcmF0b3J5IHN5bXB0b21zKSBhbmQgMiAoMC40JSkgcmVxdWlyZWQgYSBwcmltYXJ5IGNhcmUgcGh5c2ljaWFuIHJlZmVycmFsLiBJbiB0b3RhbCwgNDUgKDg2LjUlKSBVQ0NzIGNoYXJnZWQgYSBmZWUgdG8gdGVzdCB1bmluc3VyZWQgcGF0aWVudHMsIGJ1dCBubyBVQ0MgY291bGQgcHJvdmlkZSBhIGRlZmluaXRpdmUgYW5zd2VyIHJlZ2FyZGluZyB0ZXN0IGZlZXMgZm9yIGluc3VyZWQgcGF0aWVudHMgZ2l2ZW4gdGhlIHNoaWZ0aW5nIGZlZGVyYWwgbGVnaXNsYXRpb24uIEEgdG90YWwgb2YgNTMgKDk0LjYlKSBVQ0NzIGNoYXJnZWQgYSB2aXNpdCBmZWUgaW4gYWRkaXRpb24gdG8gdGhlIENPVklELTE5IGxhYiB0ZXN0IGZlZS4gRm9yIHRoZSA0OSBjZW50ZXJzIHRoYXQgcHJvdmlkZWQgdGhlIHdhaXQgdGltZSBmb3IgdGVzdCByZXN1bHRzLCB0aGUgbWVkaWFuIHRpbWUgd2FzIDEyMCBob3VycyAoaW50ZXJxdWFydGlsZSByYW5nZSA5NiBob3VycyB0byAxNDQgaG91cnMpLjwvcD48cD5PZiBVQ0NzIHRoYXQgZGlkIG5vdCBvZmZlciB0byB0ZXN0LCA5NyAoNTEuMyUpIHJlZmVycmVkIGluZGl2aWR1YWxzIHRvIG90aGVyIGNsaW5pY3MgdGhhdCBjb3VsZCBwb3NzaWJseSB0ZXN0IGZvciBDT1ZJRC0xOSwgYW5kIDM3ICgyNC44JSkgZGlyZWN0bHkgcmVmZXJyZWQgaW5kaXZpZHVhbHMgdG8gYSBzcGVjaWZpYyBlbWVyZ2VuY3kgZGVwYXJ0bWVudC4gSW5kaXZpZHVhbC1sZXZlbCByZXN1bHRzIGZvciBlYWNoIFVDQyBhcmUgYXZhaWxhYmxlIGFzIFVuZGVybHlpbmcgZGF0YTIuPC9wPiJ9XSwiZmFtaWx5IjoidGV4dCIsInZhcmlhbnQiOiJoZWFkaW5nIHBhcmFncmFwaCIsInNldHRpbmdzIjp7InBhZGRpbmdUb3AiOjMsInBhZGRpbmdCb3R0b20iOjMsImJhY2tncm91bmRDb2xvciI6IiIsImVudHJhbmNlQW5pbWF0aW9uIjp0cnVlfX0seyJpZCI6ImNrZ2dnb3U2NzAwMzgzYTYxZ2hsbnF0eGMiLCJ0eXBlIjoidGV4dCIsIml0ZW1zIjpbeyJpZCI6ImNrZ2dnMm1xZDAwMjQzYTYxbjV0cjlsdHgiLCJoZWFkaW5nIjoiPHA+RGlzY3Vzc2lvbjwvcD4iLCJwYXJhZ3JhcGgiOiI8cD5JbiB0aGUgMTAgc3RhdGVzIHdpdGggdGhlIGdyZWF0ZXN0IENPVklELTE5IGNhc2Vsb2FkLCBvbmx5IDIzJSBvZiBVQ0NzIG9mZmVyZWQgQ09WSUQtMTkgdGVzdGluZy4gQWRkaXRpb25hbGx5LCByZXN1bHRzIHdvdWxkIHRha2UgYXBwcm94aW1hdGVseSBmaXZlIGRheXMgdG8gYmUgcHJvY2Vzc2VkLiBBbHRob3VnaCB0aGUgdGltZSB0byB0ZXN0IHJlc3VsdHMgYXQgcHVibGljL3N0YXRlIGxhYnMgaXMgdHlwaWNhbGx5IDI0Jm5kYXNoOzQ4IGhvdXJzIChUYWJsZSAxKSwgdGltZSB0byB0ZXN0IHJlc3VsdHMgYXQgVUNDcyB3ZXJlIGxvbmdlciBhcyBtb3N0IHNhbXBsZXMgYXJlIHNlbnQgdG8gZXh0ZXJuYWwgbGFicy4gSG93ZXZlciwgaXQgcmVtYWlucyB1bmNsZWFyIHdoZXRoZXIgVUNDJiMzOTtzIGFiaWxpdHkgdG8gb2J0YWluIHRlc3Qgc2FtcGxlcyBtYXkgYmUgdW5tYXRjaGVkIGJ5IHRoZSBhYmlsaXR5IHRvIHByb2Nlc3MgdGVzdHMuIFRoaXMgZmluZGluZyB1bmRlcnNjb3JlcyB0aGUgaW1wb3J0YW5jZSBvZiBwb2ludC1vZi1jYXJlIHRlc3RpbmcgdGhhdCBjYW4gcmFwaWRseSBkZXRlY3QgQ09WSUQtMTksIHBhcnRpY3VsYXJseSBiZWNhdXNlIG9mIHNldmVyZSBkaXNlYXNlIHBlYWtzIGF0IGFwcHJveGltYXRlbHkgdGVuIGRheXMgZnJvbSB0aGUgb25zZXQgb2YgaW5pdGlhbCBzeW1wdG9tczMuJm5ic3A7PC9wPjxwPkZlZXMgYW5kIGNvc3Qtc2hhcmluZyBmb3IgQ09WSUQtMTkgdGVzdHMgcmVtYWluIHVuY2xlYXIuIFRoZSBGYW1pbGllcyBGaXJzdCBDb3JvbmF2aXJ1cyBSZXNwb25zZSBBY3QsIHdoaWNoIHBhc3NlZCBvbiBNYXJjaCAxOCwgbWFuZGF0ZWQgYWxsIGdyb3VwIGFuZCBpbmRpdmlkdWFsIGhlYWx0aCBwbGFucyBjb3ZlciBDT1ZJRC0xOSB0ZXN0aW5nIGFuZCBnYXZlIHN0YXRlcyB0aGUgb3B0aW9uIHRvIHVzZSBNZWRpY2FpZCBjb3ZlcmFnZSBmb3IgdGVzdGluZyB1bmluc3VyZWQgcGF0aWVudHM0LiBBbHRob3VnaCB0aGlzIHN0dWR5IGNvdWxkIG5vdCBkZWZpbml0aXZlbHkgZGVmaW5lIHRlc3QgZmVlcywgbW9zdCBVQ0NzIHN0YXRlZCB0aGV5IHdvdWxkIGNoYXJnZSB0ZXN0IGZlZXMsIGNvbnRyYXJ5IHRvIHJlY2VudCBmZWRlcmFsIHJlZ3VsYXRpb25zLCBpbiBhZGRpdGlvbiB0byBmZWVzIGZvciB0aGUgdXJnZW50IGNhcmUgdmlzaXQgaXRzZWxmIGFzIG9mIE1hcmNoIDIwLiBUZXN0IGFuZCB2aXNpdCBmZWVzIGF0IFVDQ3MgbWF5IGRpc2NvdXJhZ2UgcGF0aWVudHMgZnJvbSBzZWVraW5nIENPVklELTE5IHRlc3RpbmcuPC9wPjxwPlRoaXMgcmVwb3J0IGhhcyBsaW1pdGF0aW9ucy4gVGhlIHNtYWxsIG51bWJlciBvZiBVQ0NzIGNvbnRhY3RlZCBwZXIgc3RhdGUgbWF5IG5vdCBhY2N1cmF0ZWx5IHJlcHJlc2VudCB0aGUgc3RhdGUmcnNxdW87cyB1cmdlbnQgY2FyZSBjbGltYXRlLiBBZGRpdGlvbmFsbHksIHRoZSByYXBpZGx5IGNoYW5naW5nIG5hdHVyZSBvZiB0aGUgQ09WSUQtMTkgcGFuZGVtaWMgbWF5IGFmZmVjdCB0aGVzZSBmaW5kaW5ncy4gSG93ZXZlciwgdGhpcyBzdHVkeSBzZXJ2ZXMgYXMgYW4gaW1wb3J0YW50IHNuYXBzaG90IHRoYXQgaGlnaGxpZ2h0cyB0aGUgbGltaXRlZCBDT1ZJRC0xOSB0ZXN0aW5nIGNhcGFiaWxpdGllcyBhdCBVQ0NzIGluIHRoZSBtb3N0IGhlYXZpbHkgYnVyZGVuZWQgc3RhdGVzLjwvcD48cD48YnI+PC9wPiJ9XSwiZmFtaWx5IjoidGV4dCIsInZhcmlhbnQiOiJoZWFkaW5nIHBhcmFncmFwaCIsInNldHRpbmdzIjp7InBhZGRpbmdUb3AiOjMsInBhZGRpbmdCb3R0b20iOjMsImJhY2tncm91bmRDb2xvciI6IiIsImVudHJhbmNlQW5pbWF0aW9uIjp0cnVlfX1dLCJtZWRpYSI6e30sInBpbGVzIjpbXSwic2V0dGluZ3MiOnt9LCJkdXBsaWNhdGVkRnJvbUlkIjoiIiwiZGVsZXRlZCI6ZmFsc2UsImNyZWF0ZWRBdCI6IjIwMjAtMTAtMTlUMTE6MTg6MjUuNjM4WiIsInVwZGF0ZWRBdCI6IjIwMjAtMTAtMTlUMTE6NDc6MTMuODQ1WiIsImxhc3RVcGRhdGVkQnkiOiJhaWR8Y2I0M2Y1M2QtYjliNi00MjhkLTkyYzgtMjZmOGUzY2JlNWYxIiwicmVhZHkiOnRydWUsInBvc2l0aW9uIjpudWxsLCJ0cmFuc2ZlcnJlZEF0IjpudWxsLCJkdXJhdGlvbiI6M31dLCJqb2JzIjpbXSwibGFiZWxTZXRJZCI6IiIsImRlbGV0ZWQiOmZhbHNlLCJjcmVhdGVkQXQiOiIyMDIwLTEwLTE5VDExOjE3OjMzLjQyMVoiLCJ1cGRhdGVkQXQiOiIyMDIwLTEwLTE5VDExOjE4OjI1LjYzOFoiLCJtYXJrQ29tcGxldGUiOmZhbHNlLCJpc0RlZmF1bHQiOmZhbHNlLCJyZWFkeSI6dHJ1ZSwiZXhwb3J0U2V0dGluZ3MiOnt9LCJyZXZpZXdJZCI6IiIsImhlYWRpbmdUeXBlZmFjZUlkIjoiX3Z5OUlWcVJQd3o2d1FsS2kwd3NBOWRzWXBaN2dzUTMiLCJib2R5VHlwZWZhY2VJZCI6IldzdHUxbFZrUl9yVXNBdVU2eDFOX1VJbXdjaHp5Z2oyIiwidWlUeXBlZmFjZUlkIjoiX3Z5OUlWcVJQd3o2d1FsS2kwd3NBOWRzWXBaN2dzUTMiLCJzaWRlYmFyTW9kZSI6Im9wZW4iLCJ0ZW5hbnRJZCI6Ijg4NjI5M2EwLWNkMTQtNDBmOC1hZmY2LWU0NTAwYTljNTYzMSIsInNob3dMZXNzb25Db3VudCI6dHJ1ZSwic2hvd05hdmlnYXRpb25CdXR0b25zIjp0cnVlLCJhbGxvd1NlYXJjaCI6dHJ1ZSwiYW5pbWF0ZUJsb2NrRW50cmFuY2UiOnRydWUsInRyYW5zZmVycmVkQXQiOm51bGwsImFsbG93Q29weSI6ZmFsc2UsImVuYWJsZVZpZGVvUGxheWJhY2tTcGVlZCI6dHJ1ZSwiam9iVHlwZSI6bnVsbCwiYXV0aG9ycyI6W3siaWQiOiJhaWR8Y2I0M2Y1M2QtYjliNi00MjhkLTkyYzgtMjZmOGUzY2JlNWYxIiwiYXZhdGFyIjoiS21vUHFHX2FEYXdLUThvMF9zbWFsbC5wbmciLCJhdXRob3JOYW1lIjoiU2hpdmFtIEphaXN3YWwifV0sImJvZHlUeXBlZmFjZSI6Ik1lcnJpd2VhdGhlciIsImhlYWRpbmdUeXBlZmFjZSI6IkxhdG8iLCJ1aVR5cGVmYWNlIjoiTGF0byIsImxtc09wdGlvbnMiOnsiZW5hYmxlRXhpdENvdXJzZSI6ZmFsc2V9fSwibGFiZWxTZXQiOnsiaWQiOiI4dDdUZzY2OHNacEhKRS02QUp0NU9JNUciLCJhdXRob3IiOiJhaWR8Y2I0M2Y1M2QtYjliNi00MjhkLTkyYzgtMjZmOGUzY2JlNWYxIiwibmFtZSI6IkVuZ2xpc2giLCJkZWZhdWx0SWQiOjEsImRlZmF1bHRTZXQiOnRydWUsImxhYmVscyI6eyJyZXN1bHQiOiJyZXN1bHQiLCJzZWFyY2giOiJzZWFyY2giLCJyZXN1bHRzIjoicmVzdWx0cyIsInF1aXpOZXh0IjoiTkVYVCIsInRhYkdyb3VwIjoiVGFiIiwiY29kZUdyb3VwIjoiQ29kZSIsIm5vUmVzdWx0cyI6Ik5vIHJlc3VsdHMgZm9yIiwibm90ZUdyb3VwIjoiTm90ZSIsInF1aXpTY29yZSI6IllvdXIgc2NvcmUiLCJxdWl6U3RhcnQiOiJTVEFSVCBRVUlaIiwiY291cnNlRXhpdCI6IkVYSVQgQ09VUlNFIiwiY291cnNlSG9tZSI6IkhvbWUiLCJsZXNzb25OYW1lIjoiTGVzc29uIiwicXVpelN1Ym1pdCI6IlNVQk1JVCIsInF1b3RlR3JvdXAiOiJRdW90ZSIsInNhbHV0YXRpb24iOiLwn5GLIEJ5ZSEiLCJidXR0b25Hcm91cCI6IkJ1dHRvbiIsImNvdXJzZVN0YXJ0IjoiU1RBUlQgQ09VUlNFIiwiZW1iZWRWaWV3T24iOiJWSUVXIE9OIiwiZXhpdE1lc3NhZ2UiOiJZb3UgbWF5IG5vdyBsZWF2ZSB0aGlzIHBhZ2UuIiwicXVpekNvcnJlY3QiOiJDb3JyZWN0IiwicXVpelBhc3NpbmciOiJQQVNTSU5HIiwicXVpelJlc3VsdHMiOiJRdWl6IFJlc3VsdHMiLCJjb3Vyc2VSZXN1bWUiOiJSRVNVTUUgQ09VUlNFIiwicHJvY2Vzc1N0YXJ0IjoiU1RBUlQiLCJwcm9jZXNzU3dpcGUiOiJTd2lwZSB0byBjb250aW51ZSIsInF1aXpDb250aW51ZSI6IkNvbnRpbnVlIiwicXVpekxhbmRtYXJrIjoiUXVpeiIsInF1aXpRdWVzdGlvbiI6IlF1ZXN0aW9uIiwiY291cnNlRGV0YWlscyI6IkRFVEFJTFMiLCJlbWJlZFJlYWRNb3JlIjoiUmVhZCBtb3JlIiwiZmVlZGJhY2tHcm91cCI6IkZlZWRiYWNrIiwicXVpekluY29ycmVjdCI6IkluY29ycmVjdCIsInF1aXpUYWtlQWdhaW4iOiJUQUtFIEFHQUlOIiwic29ydGluZ1JlcGxheSI6IlJFUExBWSIsImFjY29yZGlvbkdyb3VwIjoiQWNjb3JkaW9uIiwiZW1iZWRMaW5rR3JvdXAiOiJFbWJlZGRlZCB3ZWIgY29udGVudCIsImxlc3NvbkNvbXBsZXRlIjoiQ09NUExFVEUiLCJzdGF0ZW1lbnRHcm91cCI6IlN0YXRlbWVudCIsInN0b3J5bGluZUdyb3VwIjoiU3RvcnlsaW5lIiwiYXR0YWNobWVudEdyb3VwIjoiRmlsZSBBdHRhY2htZW50IiwiZW1iZWRQaG90b0dyb3VwIjoiRW1iZWRkZWQgcGhvdG8iLCJlbWJlZFZpZGVvR3JvdXAiOiJFbWJlZGRlZCB2aWRlbyIsInBsYXlCdXR0b25MYWJlbCI6IlBsYXkiLCJwcm9jZXNzQ29tcGxldGUiOiJDb21wbGV0ZSIsInByb2Nlc3NMYW5kbWFyayI6IlByb2Nlc3MiLCJwcm9jZXNzTmV4dFN0ZXAiOiJORVhUIFNURVAiLCJwcm9jZXNzU3RlcE5hbWUiOiJTdGVwIiwic2Vla1NsaWRlckxhYmVsIjoiU2VlayIsInNvcnRpbmdMYW5kbWFyayI6IlNvcnRpbmcgQWN0aXZpdHkiLCJhdWRpb1BsYXllckdyb3VwIjoiQXVkaW8gcGxheWVyLiBZb3UgY2FuIHVzZSB0aGUgc3BhY2UgYmFyIHRvIHRvZ2dsZSBwbGF5YmFjayBhbmQgYXJyb3cga2V5cyB0byBzY3J1Yi4iLCJidXR0b25TdGFja0dyb3VwIjoiQnV0dG9uIHN0YWNrIiwiZW1iZWRQbGF5ZXJHcm91cCI6IkVtYmVkZGVkIG1lZGlhIHBsYXllciIsImxlc3NvblJlc3RyaWN0ZWQiOiJMZXNzb25zIG11c3QgYmUgY29tcGxldGVkIGluIG9yZGVyIiwicGF1c2VCdXR0b25MYWJlbCI6IlBhdXNlIiwic2NlbmFyaW9Db21wbGV0ZSI6IlNjZW5hcmlvIENvbXBsZXRlISIsInNjZW5hcmlvQ29udGludWUiOiJDT05USU5VRSIsInNjZW5hcmlvVHJ5QWdhaW4iOiJUUlkgQUdBSU4iLCJ0ZXh0T25JbWFnZUdyb3VwIjoiVGV4dCBvbiBpbWFnZSIsInRpbWVsaW5lTGFuZG1hcmsiOiJUaW1lbGluZSIsInVybEVtYmVkTGFuZG1hcmsiOiJVUkwvRW1iZWQiLCJ2aWRlb1BsYXllckdyb3VwIjoiVmlkZW8gcGxheWVyLiBZb3UgY2FuIHVzZSB0aGUgc3BhY2UgYmFyIHRvIHRvZ2dsZSBwbGF5YmFjayBhbmQgYXJyb3cga2V5cyB0byBzY3J1Yi4iLCJibG9ja3NDbGlja1RvRmxpcCI6IkNsaWNrIHRvIGZsaXAiLCJibG9ja3NQcmVwb3NpdGlvbiI6Im9mIiwiYnVsbGV0ZWRMaXN0R3JvdXAiOiJCdWxsZXRlZCBsaXN0IiwiY2hlY2tib3hMaXN0R3JvdXAiOiJDaGVja2JveCBsaXN0IiwiaW1hZ2VBbmRUZXh0R3JvdXAiOiJJbWFnZSBhbmQgdGV4dCIsImltYWdlR2FsbGVyeUdyb3VwIjoiSW1hZ2UgZ2FsbGVyeSIsImxlc3NvblByZXBvc2l0aW9uIjoib2YiLCJudW1iZXJlZExpc3RHcm91cCI6Ik51bWJlcmVkIGxpc3QiLCJwcm9jZXNzTGVzc29uTmFtZSI6Ikxlc3NvbiIsInByb2Nlc3NTdGFydEFnYWluIjoiU1RBUlQgQUdBSU4iLCJzY2VuYXJpb1N0YXJ0T3ZlciI6IlNUQVJUIE9WRVIiLCJjb3Vyc2VTa2lwVG9MZXNzb24iOiJTS0lQIFRPIExFU1NPTiIsImZsYXNoY2FyZEJhY2tMYWJlbCI6IkZsYXNoY2FyZCBiYWNrIiwiZmxhc2hjYXJkR3JpZEdyb3VwIjoiRmxhc2hjYXJkIGdyaWQiLCJuZXh0Rmxhc2hDYXJkTGFiZWwiOiJOZXh0IGZsYXNoY2FyZCIsImZsYXNoY2FyZEZyb250TGFiZWwiOiJGbGFzaGNhcmQgZnJvbnQiLCJmbGFzaGNhcmRTdGFja0dyb3VwIjoiRmxhc2hjYXJkIHN0YWNrIiwia25vd2xlZGdlQ2hlY2tHcm91cCI6Iktub3dsZWRnZSBjaGVjayIsInNvcnRpbmdDYXJkc0NvcnJlY3QiOiJDYXJkcyBDb3JyZWN0IiwiaGFtYnVyZ2VyQnV0dG9uTGFiZWwiOiJDb3Vyc2UgT3ZlcnZpZXcgU2lkZWJhciIsImxlc3NvbkhlYWRlckxhbmRtYXJrIjoiTGVzc29uIEhlYWRlciIsIm51bWJlcmVkRGl2aWRlckdyb3VwIjoiTnVtYmVyZWQgZGl2aWRlciIsImxlc3NvbkNvbnRlbnRMYW5kbWFyayI6Ikxlc3NvbiBDb250ZW50IiwibGVzc29uU2lkZWJhckxhbmRtYXJrIjoiTGVzc29uIFNpZGViYXIiLCJxdWl6QW5zd2VyUGxhY2Vob2xkZXIiOiJUeXBlIHlvdXIgYW5zd2VyIGhlcmUiLCJsYWJlbGVkR3JhcGhpY0xhbmRtYXJrIjoiTGFiZWxlZCBHcmFwaGljIiwicHJldmlvdXNGbGFzaENhcmRMYWJlbCI6IlByZXZpb3VzIGZsYXNoY2FyZCIsInByb2Nlc3NTdGVwUHJlcG9zaXRpb24iOiJvZiIsIm92ZXJ2aWV3UGFnZVRpdGxlU3VmZml4IjoiT3ZlcnZpZXciLCJxdWl6QWNjZXB0YWJsZVJlc3BvbnNlcyI6IkFjY2VwdGFibGUgcmVzcG9uc2VzIiwicXVpelJlcXVpcmVQYXNzaW5nU2NvcmUiOiJNdXN0IHBhc3MgcXVpeiBiZWZvcmUgY29udGludWluZyIsInRpbWVsaW5lQ2FyZEdyb3VwUHJlZml4IjoiVGltZWxpbmUgQ2FyZCIsImxhYmVsZWRHcmFwaGljQnViYmxlTGFiZWwiOiJMYWJlbGVkIGdyYXBoaWMgYnViYmxlIiwibGFiZWxlZEdyYXBoaWNNYXJrZXJMYWJlbCI6IkxhYmVsZWQgZ3JhcGhpYyBtYXJrZXIiLCJsYWJlbGVkR3JhcGhpY05leHRNYXJrZXJMYWJlbCI6Ik5leHQgbWFya2VyIiwibGFiZWxlZEdyYXBoaWNQcmV2aW91c01hcmtlckxhYmVsIjoiUHJldmlvdXMgbWFya2VyIn0sImRlbGV0ZWQiOmZhbHNlLCJjcmVhdGVkQXQiOiIyMDIwLTEwLTE2VDEwOjQzOjEzLjU1NloiLCJ1cGRhdGVkQXQiOiIyMDIwLTEwLTE2VDEwOjQzOjEzLjU1NloiLCJpc282MzlDb2RlIjoiZW4iLCJ0cmFuc2ZlcnJlZEF0IjpudWxsfSwiZm9udHMiOlt7ImlkIjoiX3Z5OUlWcVJQd3o2d1FsS2kwd3NBOWRzWXBaN2dzUTMiLCJuYW1lIjoiTGF0byIsImRlZmF1bHQiOnRydWUsImtleSI6ImFzc2V0cy9yaXNlL2ZvbnRzL0xhdG8tTGlnaHQud29mZiIsInN0eWxlIjoibm9ybWFsIiwid2VpZ2h0IjoiMzAwIn0seyJpZCI6Il92eTlJVnFSUHd6NndRbEtpMHdzQTlkc1lwWjdnc1EzIiwibmFtZSI6IkxhdG8iLCJkZWZhdWx0Ijp0cnVlLCJrZXkiOiJhc3NldHMvcmlzZS9mb250cy9MYXRvLVJlZ3VsYXIud29mZiIsInN0eWxlIjoibm9ybWFsIiwid2VpZ2h0IjoiNDAwIn0seyJpZCI6Il92eTlJVnFSUHd6NndRbEtpMHdzQTlkc1lwWjdnc1EzIiwibmFtZSI6IkxhdG8iLCJkZWZhdWx0Ijp0cnVlLCJrZXkiOiJhc3NldHMvcmlzZS9mb250cy9MYXRvLUJvbGQud29mZiIsInN0eWxlIjoibm9ybWFsIiwid2VpZ2h0IjoiNzAwIn0seyJpZCI6Il92eTlJVnFSUHd6NndRbEtpMHdzQTlkc1lwWjdnc1EzIiwibmFtZSI6IkxhdG8iLCJkZWZhdWx0Ijp0cnVlLCJrZXkiOiJhc3NldHMvcmlzZS9mb250cy9MYXRvLUl0YWxpYy53b2ZmIiwic3R5bGUiOiJpdGFsaWMiLCJ3ZWlnaHQiOiI0MDAifSx7ImlkIjoiX3Z5OUlWcVJQd3o2d1FsS2kwd3NBOWRzWXBaN2dzUTMiLCJuYW1lIjoiTGF0byIsImRlZmF1bHQiOnRydWUsImtleSI6ImFzc2V0cy9yaXNlL2ZvbnRzL0xhdG8tQmxhY2sud29mZiIsInN0eWxlIjoibm9ybWFsIiwid2VpZ2h0IjoiOTAwIn0seyJpZCI6IldzdHUxbFZrUl9yVXNBdVU2eDFOX1VJbXdjaHp5Z2oyIiwibmFtZSI6Ik1lcnJpd2VhdGhlciIsImRlZmF1bHQiOnRydWUsImtleSI6ImFzc2V0cy9yaXNlL2ZvbnRzL01lcnJpd2VhdGhlci1MaWdodC53b2ZmIiwic3R5bGUiOiJub3JtYWwiLCJ3ZWlnaHQiOiIzMDAifSx7ImlkIjoiV3N0dTFsVmtSX3JVc0F1VTZ4MU5fVUltd2NoenlnajIiLCJuYW1lIjoiTWVycml3ZWF0aGVyIiwiZGVmYXVsdCI6dHJ1ZSwia2V5IjoiYXNzZXRzL3Jpc2UvZm9udHMvTWVycml3ZWF0aGVyLVJlZ3VsYXIud29mZiIsInN0eWxlIjoibm9ybWFsIiwid2VpZ2h0IjoiNDAwIn0seyJpZCI6IldzdHUxbFZrUl9yVXNBdVU2eDFOX1VJbXdjaHp5Z2oyIiwibmFtZSI6Ik1lcnJpd2VhdGhlciIsImRlZmF1bHQiOnRydWUsImtleSI6ImFzc2V0cy9yaXNlL2ZvbnRzL01lcnJpd2VhdGhlci1Cb2xkLndvZmYiLCJzdHlsZSI6Im5vcm1hbCIsIndlaWdodCI6IjcwMCJ9LHsiaWQiOiJXc3R1MWxWa1JfclVzQXVVNngxTl9VSW13Y2h6eWdqMiIsIm5hbWUiOiJNZXJyaXdlYXRoZXIiLCJkZWZhdWx0Ijp0cnVlLCJrZXkiOiJhc3NldHMvcmlzZS9mb250cy9NZXJyaXdlYXRoZXItSXRhbGljLndvZmYiLCJzdHlsZSI6Iml0YWxpYyIsIndlaWdodCI6IjQwMCJ9LHsiaWQiOiJXc3R1MWxWa1JfclVzQXVVNngxTl9VSW13Y2h6eWdqMiIsIm5hbWUiOiJNZXJyaXdlYXRoZXIiLCJkZWZhdWx0Ijp0cnVlLCJrZXkiOiJhc3NldHMvcmlzZS9mb250cy9NZXJyaXdlYXRoZXItQmxhY2sud29mZiIsInN0eWxlIjoibm9ybWFsIiwid2VpZ2h0IjoiOTAwIn1dLCJtZWRpYSI6W119";

    var quizId = null;
    var storylineId = null;
    var completionPercentage = 100;
    var reporting = 'passed-incomplete';

    var stuffToPick = [
      'SetBookmark',   // set identifier for bookmark
      'SetDataChunk',  // set a stringified object with lesson progress
      'GetDataChunk',  // retreive stringified object with lesson progress
      'SetReachedEnd', // tell LMS user completed course
      'SetFailed', // tell LMS user Failed course
      'SetPassed', // tell LMS user Passed the course
      'SetScore', // Report Users Score to the LMS (for Pass and fail)
      'GetStatus', // Get Current Status
      'ResetStatus', // Reset the Status
      'CreateResponseIdentifier', // Create question response
      'MatchingResponse', // Create question matching response
      'RecordMatchingInteraction', // Question Type
      'RecordFillInInteraction', // Question Type
      'RecordMultipleChoiceInteraction', // Question Type
      'Finish',
      'ConcedeControl'
    ];

    var LMSProxy = pick(stuffToPick, window.parent, Function.prototype);

    finish(0);

    var cache = getLMSData();

    function debounce(fn, delay) {
  var timer = null;

  return function () {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
}

function compress(data) {
  try {
    return JSON.stringify({
      v: 1,
      d: lzwCompress.pack(JSON.stringify(data))
    });
  } catch(e) {
    return '';
  }
}

function decompress(stringData) {
  try {
    const data = JSON.parse(stringData);

    return data.d
      ? JSON.parse(lzwCompress.unpack(data.d))
      : data;
  } catch(e) {
    return {};
  }
}

function assign(target) {
  if (target === undefined || target === null) {
    throw new TypeError('assign: Cannot convert undefined or null to object');
  }

  var output = Object(target);
  
    <![CDATA[
    for (var index = 1; index < arguments.length; index++) {
    var source = arguments[index];
    if (source !== undefined && source !== null) {
      for (var nextKey in source) {
        if (source.hasOwnProperty(nextKey)) {
          output[nextKey] = source[nextKey];
        }
      }
    }
  }
]]>
  
  return output;
}

function identity(value) {
  return value;
}

function bookmark(id) {
  var url = 'index.html#/lessons/' + id;

  LMSProxy.SetBookmark(url, '');
}

function completeOut(passed, reportParam) {
  var reportType = reportParam || reporting;

  if(passed) {
    switch(reportType) {
      case 'completed-incomplete':
      case 'completed-failed':
        LMSProxy.ResetStatus();
        LMSProxy.SetReachedEnd();
        break;

      case 'passed-incomplete':
      case 'passed-failed':
        LMSProxy.SetPassed();
        LMSProxy.SetReachedEnd();
        break;
    }
  } else {
    switch(reportType) {
      case 'passed-failed':
      case 'completed-failed':
        if(!isPassed()) {
          LMSProxy.SetFailed();
        }
        break;
    }
  }
}

var setDataChunk = debounce(function(data) {
  LMSProxy.SetDataChunk(data);
}, 1000);

function concatLMSData(data) {
  assign(cache, data);
  setDataChunk(compress(cache));
}

function exit() {
  LMSProxy.ConcedeControl();
}
<![CDATA[
function finish(totalProgress) {
  if (
    quizId === null &&
    storylineId === null &&
    completionPercentage != undefined &&
    totalProgress >= completionPercentage
  ) {
    completeOut(true);
  }
}
]]>
function finishQuiz(passed, score, id) {
  if (id == quizId) {
    reportScore(score);
    completeOut(passed);
  }
}

function finishStoryline(id, passed, isQuiz, score) {
  if (id == storylineId) {
    if (isQuiz) {
      reportScore(score);
      completeOut(passed, 'passed-failed');
    } else {
      completeOut(passed);
    }
  }
}
<![CDATA[
function stripHtml(title) {
  return title.replace(/<(?:.|\n)*?>/gm, '');
}
]]>
function shorten(title) {
  return title.charAt(0);
}

function buildResponseIdentifier(response) {
  var title = stripHtml(response.title);
  return LMSProxy.CreateResponseIdentifier(shorten(title), title);
}

function buildMatchingResponse(response) {
  var sourceTitle = stripHtml(response.source.title);
  var targetTitle = stripHtml(response.target.title);

  var source =
    LMSProxy.CreateResponseIdentifier(shorten(sourceTitle), sourceTitle);
  var target =
    LMSProxy.CreateResponseIdentifier(shorten(targetTitle), targetTitle);

  return new LMSProxy.MatchingResponse(source, target);
}

function cleanTitle(title) {
  return title.substr(0, 20).replace(/ /g, '_');
}

function titleToId(quiz, question, attempts) {
  return cleanTitle(quiz) + '_' + cleanTitle(question) + '_' + attempts;
}

function normalizeResult(correct) {
  return correct
    ? window.parent.INTERACTION_RESULT_CORRECT
    : window.parent.INTERACTION_RESULT_WRONG;
}

function reportAnswer(data) {
  var recorder = Function.prototype;
  var response;
  var correctResponses;

  var isCorrect = normalizeResult(data.isCorrect);
  var latency = data.latency;
  var title = data.questionTitle;
  var weighting = 1;
  var strId = titleToId(data.quizTitle, title, data.retryAttempts);

  switch (data.type) {
    case 'MULTIPLE_CHOICE':
    case 'MULTIPLE_RESPONSE':
      response = data.response.map(buildResponseIdentifier);
      correctResponses = data.correctResponse.map(buildResponseIdentifier);
      recorder = LMSProxy.RecordMultipleChoiceInteraction;
      break;

    case 'FILL_IN_THE_BLANK':
      response = data.response;
      correctResponses = data.correctResponse;
      recorder = LMSProxy.RecordFillInInteraction;
      break;

    case 'MATCHING':
      response = data.response.map(buildMatchingResponse);
      correctResponses = data.correctResponse.map(buildMatchingResponse);
      recorder = LMSProxy.RecordMatchingInteraction;
      break;
  }

  recorder(
    strId,
    response,
    isCorrect,
    correctResponses,
    title,
    weighting,
    latency,
    0
  );
}

function getLMSData() {
  var data = null;

  if(!cache) {
    data = LMSProxy.GetDataChunk();

    var result = data
      ? decompress(data)
      : {};

    return result;
  }

  return assign({}, cache);
}

function getProgress(initialProgress) {
  var decodeCourseProgress =
    root.Rise.decodeCourseProgress;

  var progress = assign({}, {
    progress: { lessons: {} }
  }, getLMSData()).progress;

  if(initialProgress) {
    return decodeCourseProgress(initialProgress, progress);
  }

  return progress;
}

function isExport() {
  return true;
}

function isPassed() {
  return LMSProxy.GetStatus() === window.parent.LESSON_STATUS_PASSED;
}

function pick(keys, obj, def) {
  return keys.reduce(function(memo, key) {
    if(obj[key] === undefined) {
      if(def !== undefined) {
        memo[key] = def;
      }
    } else {
      memo[key] = obj[key];
    }

    return memo;
  }, {});
}

function reportScore(score) {
  var highScore = Math.max(getLMSData().score || 0, score);
  concatLMSData({ score: highScore });

  LMSProxy.SetScore(highScore, 100, 0);
}

function resolvePath(path) {
  return ('assets/').concat(path);
}

function resolveFontPath(font) {
  return ('lib/fonts/').concat(font.key.split('/').reverse()[0]);
}

function setLessonProgress(lessonProgress) {
  var encodeLessonProgress =
    root.Rise.encodeLessonProgress;

  var currentProgress = getProgress();

  var lessons = assign(
    {},
    currentProgress.lessons,
    encodeLessonProgress(lessonProgress)
  );

  var progress = assign({}, currentProgress, { lessons: lessons });

  concatLMSData({ progress: progress });
}

function setCourseProgress(courseProgress) {
  var encodeCourseProgress =
    root.Rise.encodeCourseProgress;

  var currentProgress = getProgress();

  var progress = assign(
    {},
    currentProgress,
    encodeCourseProgress(courseProgress)
  );

  concatLMSData({ progress: progress });
}

function getMasteryScore(payload) {
  return payload.passingScore;
}

root.Rise = {
  completeLessons: identity,
  decodeCourseProgress: identity,
  encodeCourseProgress: identity,
  encodeLessonProgress: identity
};

root.Runtime = {
  bookmark: bookmark,
  exit: exit,
  finish: finish,
  finishQuiz: finishQuiz,
  finishStoryline: finishStoryline,
  getMasteryScore: getMasteryScore,
  getProgress: getProgress,
  isExport: isExport,
  reportAnswer: reportAnswer,
  resolveFontPath: resolveFontPath,
  resolvePath: resolvePath,
  setCourseProgress: setCourseProgress,
  setLessonProgress: setLessonProgress
};

  }(window));

                </script>
                <script src="lib/main.bundle.js"></script>
                <div class="lesson-progress-wrapper"><div class="progress-wrap"><div class="progress"><div class="progress__container"><div class="progress__indicator brand--background" style="transform: translate3d(100%, 0px, 0px);"></div></div></div></div></div>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="front/article-meta">
        <xsl:apply-templates select="title-group"/>
        <xsl:apply-templates select="abstract"/>
        
    </xsl:template>
    
    <xsl:template match="body">
        <xsl:apply-templates select="sec"/>
    </xsl:template>
    
    
    <xsl:template match="title-group">
        <div class="noOutline" data-block-id="ckggg4m4o002z3a61zkwlxirr" tabindex="-1">
            <div>
                <div class="block-text block-text--onecol" style="padding-top: 30px; padding-bottom: 30px;">
                    <div class="block-text__container">
                        <div class="block-text__row">
                            <div class="block-text__col brand--head">
                                <h2>
                                    <div class="brand--linkColor">
                                        <div aria-hidden="false" class=" brand--linkColor">
                                            <div class="fr-view">
                                                <p><strong><xsl:value-of select="article-title"/></strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </h2>
                            </div>
                        </div>
                        <div class="block-text__row">
                            <div aria-hidden="false" class="block-text__col brand--body brand--linkColor">
                                <div class="fr-view">
                                    <p><xsl:value-of select="fn-group/fn"/></p>
                                    <p><br/></p>
                                    <p><br/></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="abstract">
        <div class="noOutline" data-block-id="ckgggh49700333a61ktm3y477" tabindex="-1"><div><div class="block-text block-text--onecol" style="padding-top: 30px; padding-bottom: 30px;"><div class="block-text__container"><div class="block-text__row"><div class="block-text__col brand--head"><h2><div class="brand--linkColor"><div aria-hidden="false" class=" brand--linkColor"><div class="fr-view"><p>Abstract</p></div></div></div></h2></div></div><div class="block-text__row"><div aria-hidden="false" class="block-text__col brand--body brand--linkColor"><div class="fr-view"><p><xsl:value-of select="p"/></p></div></div></div></div></div></div></div>
    </xsl:template>
    
    <xsl:template match="sec">
        <xsl:text>element with no attribute</xsl:text>
        <br/>
    </xsl:template>
    <xsl:template match="sec[@sec-type='intro']">
        <div class="noOutline" data-block-id="ckgggj1uj00353a61btmk5f8l" tabindex="-1"><div><div class="block-text block-text--onecol" style="padding-top: 30px; padding-bottom: 30px;"><div class="block-text__container"><div class="block-text__row"><div class="block-text__col brand--head"><h2><div class="brand--linkColor"><div aria-hidden="false" class=" brand--linkColor"><div class="fr-view"><p><xsl:value-of select="title"/></p></div></div></div></h2></div></div><div class="block-text__row"><div aria-hidden="false" class="block-text__col brand--body brand--linkColor"><div class="fr-view"><p><xsl:value-of select="p"/></p></div></div></div></div></div></div></div>
    </xsl:template> 
    <xsl:template match="sec[@sec-type='methods']">
        <div class="noOutline" data-block-id="ckgggmgfi00363a613447ud6i" tabindex="-1"><div><div class="block-text block-text--onecol" style="padding-top: 30px; padding-bottom: 30px;"><div class="block-text__container"><div class="block-text__row"><div class="block-text__col brand--head"><h2><div class="brand--linkColor"><div aria-hidden="false" class=" brand--linkColor"><div class="fr-view"><p><xsl:value-of select="title"/></p></div></div></div></h2></div></div><div class="block-text__row"><div aria-hidden="false" class="block-text__col brand--body brand--linkColor"><div class="fr-view"><p>Our study received non-human research IRB exemption from the Yale School of Medicine and participant consent was not required. We identified ten states with the highest COVID-19 caseload as of March 19, 2020 according to the Centers for Disease Control (CDC)1. Using the Urgent Care Association “Find an Urgent Care” directory, we identified all UCCs within the state of interest and assigned each UCC a numeric identifier. A random number generator was used to select for a convenience sample of 25 UCCs per state. If the UCC was not able to be contacted, a new UCC was randomly selected and called. UCCs were classified into independent, hospital/health network, and academic categories.</p><p>Using a standardized survey script (Figure 1), trained investigators asked UCC receptionists about COVID-19 testing ability, testing criteria, time to test results, costs of tests and visits for insured/uninsured patients, and test referrals. All 250 calls were made on March 20, 2020 and were limited to 1 minute to minimize occupying clinic resources.</p></div></div></div></div></div></div></div>
        <div class="noOutline" data-block-id="ckgggz8vv003e3a6134210pae" tabindex="-1">
            <div>
                <div class="block-image block-image--hero" style="padding-top: 30px; padding-bottom: 30px;">
                    <div class="block-image__container">
                        <div class="block-image__row">
                            <div class="block-image__col">
                                <div>
                                    <div class="animated fadeIn" style="animation-duration: 0.75s; opacity: 1; animation-delay: 0s;">
                                        <figure aria-labelledby="figcaption-ckggg2mqd002g3a61xnk7fe70" class="block-image__figure" role="figure">
                                            <div class="block-image__image">
                                                <div class="img">
                                                    <img alt="Figure 1." class="img-img img-zoom" role="img" src="assets/k2_gFCt1Z-sR40v6_stj0e07H6LbSq15V.gif"/>
                                                        <button aria-label="Zoom image" class="img-btn img-zoom visually-hidden">
                                                            <svg aria-hidden="true" class="img-btn-symbol" focusable="false" role="img" viewBox="0 0 493 493" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M72.1125 306.317L102.113 306.318L102.112 369.127L369.124 102.115L306.316 102.115L306.315 72.1142L420.31 72.1149L420.311 186.11L390.311 186.109L390.311 123.355L123.352 390.313L186.107 390.312L186.108 420.313L72.1125 420.313V306.317Z">
                                                                </path></svg>
                                                        </button>
                                                </div>
                                            </div>
                                            <figcaption id="figcaption-ckggg2mqd002g3a61xnk7fe70">
                                                <div aria-hidden="false" class="block-image__caption brand--body brand--linkColor brand--linkColor">
                                                    <div class="fr-view">
                                                        <p>Figure 1.</p>
                                                    </div>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="sec[@sec-type='results']">
        <div class="noOutline" data-block-id="ckgggnd7p00373a61g1ud033v" tabindex="-1"><div><div class="block-text block-text--onecol" style="padding-top: 30px; padding-bottom: 30px;"><div class="block-text__container"><div class="block-text__row"><div class="block-text__col brand--head"><h2><div class="brand--linkColor"><div aria-hidden="false" class=" brand--linkColor"><div class="fr-view"><p><xsl:value-of select="title"/></p></div></div></div></h2></div></div><div class="block-text__row"><div aria-hidden="false" class="block-text__col brand--body brand--linkColor"><div class="fr-view"><p><xsl:for-each select="p">
            <xsl:value-of select="."/>
            <br/>
            <br/>
            </xsl:for-each>
            </p></div></div></div></div></div></div></div>
    </xsl:template>
    <xsl:template match="sec[@sec-type='discussion']">
        <div class="noOutline" data-block-id="ckgggou6700383a61ghlnqtxc" tabindex="-1"><div><div class="block-text block-text--onecol" style="padding-top: 30px; padding-bottom: 30px;"><div class="block-text__container"><div class="block-text__row"><div class="block-text__col brand--head"><h2><div class="brand--linkColor"><div aria-hidden="false" class=" brand--linkColor"><div class="fr-view"><p><xsl:value-of select="title"/></p></div></div></div></h2></div></div><div class="block-text__row"><div aria-hidden="false" class="block-text__col brand--body brand--linkColor"><div class="fr-view"><p><xsl:for-each select="p">
            <xsl:value-of select="."/>
            <br/>
            <br/>
            </xsl:for-each></p></div></div></div></div></div></div></div>
    </xsl:template>
    
</xsl:stylesheet>